package org.launchcode.launchcart.models;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by LaunchCode
 */
public class Cart extends AbstractEntity {

    private List<Item> items = new ArrayList<>();

    private double total = 0.0;

    public List<Item> getItems() {
        return items;
    }

    public void addItem(Item item) {

        items.add(item);
        this.computeTotal();
    }

    public void removeItem(Item item) {

        items.remove(item);
        this.computeTotal();
    }



    private void computeTotal() {

        double total = 0.0;
        for (Item item : items) {
            total += item.getPrice();
        }

        this.total = total;
    }

    public double getTotal() {
        return total;
    }
}
